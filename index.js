(async () => {
    const mockttp = require('mockttp');

    const server = mockttp.getLocal({
        https: {
            keyPath: './key.pem',
            certPath: './cert.pem'
        }
    });

    
    await server.anyRequest().thenPassThrough({forwarding:{targetHost:"strapi.reddit.com", updateHostHeader: false},
    beforeResponse: (response) => {
        console.log(`Got ${response.statusCode} response with body: ${response.body.text}`);
    }
});
    await server.start();

    // const caFingerprint = mockttp.generateSPKIFingerprint(https.cert);

    if (process.argv[2] === 'chrome') {
        // Launch an intercepted Chrome using this proxy:
        const launchChrome = require('./launch-chrome');
        launchChrome("https://example.com", server, caFingerprint);
    } else {
        // Print out the server details for manual configuration:
        console.log(`Server running on port ${server.port}`);
    }
})(); // (All run in an async wrapper, so we can easily use top-level await)